#!/usr/bin/env python
# coding: utf-8

# In[79]:


import cv2
import numpy as np
from matplotlib import pyplot as plt

img1 = cv2.imread('C:/Users/Major Boombi/Desktop/image-processing/Hw1/Hw1_1A.JPG',0)
img2 = cv2.imread('C:/Users/Major Boombi/Desktop/image-processing/Hw1/Hw1_1B.tif',0)
img3 = cv2.imread('C:/Users/Major Boombi/Desktop/image-processing/Hw1/Hw1_1C.tif',0)


# In[80]:


###Picture info
# print(img)
# plt.imshow(img,cmap='gray')
# plt.hist(img.ravel(),256,[0,256]); 
# plt.show()

# get image properties.
# img_info= np.shape(img)
# print(type(img_info))
# print(len(img_info))
# print(img_info)


# In[81]:


## Log Operator

# c = 255/(np.log(1+np.max(img1)))
c = 45.98590442833571

img_log1 = (np.log(img1+1))*c
img_log2 = (np.log(img2+1))*c
img_log3 = (np.log(img3+1))*c

img_log1 = np.array(img_log1,dtype=np.uint8)
img_log2 = np.array(img_log2,dtype=np.uint8)
img_log3 = np.array(img_log3,dtype=np.uint8)


# In[82]:


## Raise to the power i < 1

# img_rs1 = (np.power(img1, 0.4))*1
# img_rs2 = (np.power(img2, 0.4))*1
# img_rs3 = (np.power(img3, 0.4))*1

# img_rs1 = np.array(img_rs1,dtype=np.uint8)
# img_rs2 = np.array(img_rs2,dtype=np.uint8)
# img_rs3 = np.array(img_rs3,dtype=np.uint8)

img_rs1 = np.array(255*(img1/255)**0.4,dtype='uint8')
img_rs2 = np.array(255*(img2/255)**0.4,dtype='uint8')
img_rs3 = np.array(255*(img3/255)**0.4,dtype='uint8')


# In[83]:


## Raise to the power i > 1
# img_rss1 = (np.power(img1, 2.2))*1
# img_rss2 = (np.power(img2, 2.2))*1
# img_rss3 = (np.power(img3, 2.2))*1

# img_rss1 = np.array(img_rss1,dtype=np.uint8)
# img_rss2 = np.array(img_rss2,dtype=np.uint8)
# img_rss3 = np.array(img_rss3,dtype=np.uint8)

img_rss1 = np.array(255*(img1/255)**2.2,dtype='uint8')
img_rss2 = np.array(255*(img2/255)**2.2,dtype='uint8')
img_rss3 = np.array(255*(img3/255)**2.2,dtype='uint8')


# In[84]:


## Write output image

cv2.imwrite("C:/Users/Major Boombi/Desktop/image-processing/Hw1-solution/output/Hw1_1_5809610420_1.JPG", img_log1)
cv2.imwrite("C:/Users/Major Boombi/Desktop/image-processing/Hw1-solution/output/Hw1_1_5809610420_2.JPG", img_log2)
cv2.imwrite("C:/Users/Major Boombi/Desktop/image-processing/Hw1-solution/output/Hw1_1_5809610420_3.JPG", img_log3)

cv2.imwrite("C:/Users/Major Boombi/Desktop/image-processing/Hw1-solution/output/Hw1_1_5809610420_4.JPG", img_rs1)
cv2.imwrite("C:/Users/Major Boombi/Desktop/image-processing/Hw1-solution/output/Hw1_1_5809610420_5.JPG", img_rs2)
cv2.imwrite("C:/Users/Major Boombi/Desktop/image-processing/Hw1-solution/output/Hw1_1_5809610420_6.JPG", img_rs3)

cv2.imwrite("C:/Users/Major Boombi/Desktop/image-processing/Hw1-solution/output/Hw1_1_5809610420_7.JPG", img_rss1)
cv2.imwrite("C:/Users/Major Boombi/Desktop/image-processing/Hw1-solution/output/Hw1_1_5809610420_8.JPG", img_rss2)
cv2.imwrite("C:/Users/Major Boombi/Desktop/image-processing/Hw1-solution/output/Hw1_1_5809610420_9.JPG", img_rss3)


# In[ ]:





# In[ ]:




