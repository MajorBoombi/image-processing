#!/usr/bin/env python
# coding: utf-8

# In[2]:


import cv2
import numpy as np
from matplotlib import pyplot as plt

img = cv2.imread('C:/Users/Major Boombi/Desktop/image-processing/Hw1/whatIsThisA.jpg',0)


# In[3]:


img


# In[4]:


## Original Histogram
plt.hist(img.ravel(),256,[0,256]); 
plt.show()


# In[5]:


equ = cv2.equalizeHist(img)

plt.hist(equ.ravel(),256,[0,256]); 
plt.show()


# In[10]:


plt.subplot(121),plt.imshow(img,cmap = 'gray')
plt.title('Original Image'), plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(equ,cmap = 'gray')
plt.title('Result'), plt.xticks([]), plt.yticks([])

plt.show()


# In[14]:


blur = cv2.blur(equ,(5,5))
median = cv2.medianBlur(equ,5)
gaussian = cv2.GaussianBlur(equ,(5,5),0)

plt.subplot(131),plt.imshow(blur,cmap = 'gray')
plt.title('blur'), plt.xticks([]), plt.yticks([])
plt.subplot(132),plt.imshow(median,cmap = 'gray')
plt.title('median'), plt.xticks([]), plt.yticks([])
plt.subplot(133),plt.imshow(gaussian,cmap = 'gray')
plt.title('gaussian'), plt.xticks([]), plt.yticks([])

plt.show()


# In[20]:


canny = cv2.Canny(median,100,200)

kernelx = np.array([[1,1,1],[0,0,0],[-1,-1,-1]])
kernely = np.array([[-1,0,1],[-1,0,1],[-1,0,1]])
img_prewittx = cv2.filter2D(median, -1, kernelx)
img_prewitty = cv2.filter2D(median, -1, kernely)
prewitt = img_prewittx+img_prewitty

laplacian = cv2.Laplacian(median,cv2.CV_64F)
sobelx = cv2.Sobel(median,cv2.CV_64F,1,0,ksize=5)
sobely = cv2.Sobel(median,cv2.CV_64F,0,1,ksize=5)
sobel = sobelx+sobely 

plt.subplot(121),plt.imshow(median,cmap = 'gray')
plt.title('Original Image'), plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(sobel,cmap = 'gray')
plt.title('Edge Image'), plt.xticks([]), plt.yticks([])

plt.show()


# In[22]:


#Write output
cv2.imwrite('C:/Users/Major Boombi/Desktop/image-processing/Hw1-solution/output/Hw1_3_5809610420_1.JPG',equ)

cv2.imwrite('C:/Users/Major Boombi/Desktop/image-processing/Hw1-solution/output/Hw1_3_5809610420_2.JPG',blur)
cv2.imwrite('C:/Users/Major Boombi/Desktop/image-processing/Hw1-solution/output/Hw1_3_5809610420_3.JPG',median)
cv2.imwrite('C:/Users/Major Boombi/Desktop/image-processing/Hw1-solution/output/Hw1_3_5809610420_4.JPG',gaussian)

cv2.imwrite('C:/Users/Major Boombi/Desktop/image-processing/Hw1-solution/output/Hw1_3_5809610420_5.JPG',sobel)
cv2.imwrite('C:/Users/Major Boombi/Desktop/image-processing/Hw1-solution/output/Hw1_3_5809610420_6.JPG',prewitt)
cv2.imwrite('C:/Users/Major Boombi/Desktop/image-processing/Hw1-solution/output/Hw1_3_5809610420_7.JPG',canny)


# In[29]:


result = canny + median
cv2.imwrite('C:/Users/Major Boombi/Desktop/image-processing/Hw1-solution/output/Hw1_3_5809610420_8.JPG',result)


# In[ ]:




