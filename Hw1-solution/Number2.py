#!/usr/bin/env python
# coding: utf-8

# In[72]:


import cv2
import numpy as np
from matplotlib import pyplot as plt

img1 = cv2.imread('C:/Users/Major Boombi/Desktop/image-processing/Hw1/Hw1_2A.jpg',0)
img2 = cv2.imread('C:/Users/Major Boombi/Desktop/image-processing/Hw1/Hw1_2B.jpg',0)
img3 = cv2.imread('C:/Users/Major Boombi/Desktop/image-processing/Hw1/Hw1_2C.jpg',0)


# In[73]:


################ Part A ####################

blur1 = cv2.blur(img1,(5,5))
median1 = cv2.medianBlur(img1,5)
gaussian1 = cv2.GaussianBlur(img1,(5,5),0)

plt.subplot(121),plt.imshow(img1),plt.title('Original')
plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(gaussian1),plt.title('Blurred')
plt.xticks([]), plt.yticks([])
plt.show()

# plt.imshow(img1,cmap='gray')


# In[81]:


blur2 = cv2.blur(img2,(5,5))
median2 = cv2.medianBlur(img2,15)
gaussian2 = cv2.GaussianBlur(img2,(5,5),0)


# In[82]:


blur3 = cv2.blur(img3,(5,5))
median3 = cv2.medianBlur(img3,5)
gaussian3 = cv2.GaussianBlur(img3,(5,5),0)


# In[83]:


## Write output
cv2.imwrite("C:/Users/Major Boombi/Desktop/image-processing/Hw1-solution/output/Hw1_2_5809610420_1.JPG", blur1)
cv2.imwrite("C:/Users/Major Boombi/Desktop/image-processing/Hw1-solution/output/Hw1_2_5809610420_2.JPG", median1)
cv2.imwrite("C:/Users/Major Boombi/Desktop/image-processing/Hw1-solution/output/Hw1_2_5809610420_3.JPG", gaussian1)

cv2.imwrite("C:/Users/Major Boombi/Desktop/image-processing/Hw1-solution/output/Hw1_2_5809610420_4.JPG", blur2)
cv2.imwrite("C:/Users/Major Boombi/Desktop/image-processing/Hw1-solution/output/Hw1_2_5809610420_5.JPG", median2)
cv2.imwrite("C:/Users/Major Boombi/Desktop/image-processing/Hw1-solution/output/Hw1_2_5809610420_6.JPG", gaussian2)

cv2.imwrite("C:/Users/Major Boombi/Desktop/image-processing/Hw1-solution/output/Hw1_2_5809610420_7.JPG", blur3)
cv2.imwrite("C:/Users/Major Boombi/Desktop/image-processing/Hw1-solution/output/Hw1_2_5809610420_8.JPG", median3)
cv2.imwrite("C:/Users/Major Boombi/Desktop/image-processing/Hw1-solution/output/Hw1_2_5809610420_9.JPG", gaussian3)


# In[97]:


################ Part B ####################

## Sobel
laplacian = cv2.Laplacian(median1,cv2.CV_64F)

sobelx = cv2.Sobel(median1,cv2.CV_64F,1,0,ksize=15)

sobely = cv2.Sobel(median1,cv2.CV_64F,0,1,ksize=15)

sobel1 = sobelx+sobely 

plt.subplot(2,2,1),plt.imshow(median1,cmap = 'gray')
plt.title('Original'), plt.xticks([]), plt.yticks([])
plt.subplot(2,2,2),plt.imshow(laplacian,cmap = 'gray')
plt.title('Laplacian'), plt.xticks([]), plt.yticks([])
plt.subplot(2,2,3),plt.imshow(sobelx,cmap = 'gray')
plt.title('Sobel X'), plt.xticks([]), plt.yticks([])
plt.subplot(2,2,4),plt.imshow(sobely,cmap = 'gray')
plt.title('Sobel Y'), plt.xticks([]), plt.yticks([])
# plt.subplot(2,2,4),plt.imshow(sobel,cmap = 'gray')
# plt.title('Sobel'), plt.xticks([]), plt.yticks([])

plt.show()

plt.subplot(2,2,4),plt.imshow(sobel1,cmap = 'gray')
plt.title('Sobel'), plt.xticks([]), plt.yticks([])
plt.show()

##PRewitt
kernelx = np.array([[1,1,1],[0,0,0],[-1,-1,-1]])
kernely = np.array([[-1,0,1],[-1,0,1],[-1,0,1]])
img_prewittx = cv2.filter2D(median1, -1, kernelx)
img_prewitty = cv2.filter2D(median1, -1, kernely)
prewitt1 = img_prewittx+img_prewitty

plt.subplot(121),plt.imshow(img_prewittx,cmap = 'gray')
plt.title('img_prewitt x'), plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(img_prewitty,cmap = 'gray')
plt.title('img_prewitt y'), plt.xticks([]), plt.yticks([])

plt.show()


##Canny
canny1 = cv2.Canny(img1,500,800)

plt.subplot(121),plt.imshow(median1,cmap = 'gray')
plt.title('Original Image'), plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(canny1,cmap = 'gray')
plt.title('Edge Image'), plt.xticks([]), plt.yticks([])

plt.show()


cv2.imwrite("C:/Users/Major Boombi/Desktop/image-processing/Hw1-solution/output/Hw1_2_5809610420_10.JPG", sobel1)
cv2.imwrite("C:/Users/Major Boombi/Desktop/image-processing/Hw1-solution/output/Hw1_2_5809610420_11.JPG", prewitt1)
cv2.imwrite("C:/Users/Major Boombi/Desktop/image-processing/Hw1-solution/output/Hw1_2_5809610420_12.JPG", canny1)


# In[91]:


## Sobel
laplacian = cv2.Laplacian(gaussian2,cv2.CV_64F)

sobelx = cv2.Sobel(gaussian2,cv2.CV_64F,1,0,ksize=5)

sobely = cv2.Sobel(gaussian2,cv2.CV_64F,0,1,ksize=5)

sobel2 = sobelx+sobely 

plt.subplot(2,2,1),plt.imshow(gaussian2,cmap = 'gray')
plt.title('Original'), plt.xticks([]), plt.yticks([])
plt.subplot(2,2,2),plt.imshow(laplacian,cmap = 'gray')
plt.title('Laplacian'), plt.xticks([]), plt.yticks([])
plt.subplot(2,2,3),plt.imshow(sobelx,cmap = 'gray')
plt.title('Sobel X'), plt.xticks([]), plt.yticks([])
plt.subplot(2,2,4),plt.imshow(sobely,cmap = 'gray')
plt.title('Sobel Y'), plt.xticks([]), plt.yticks([])
# plt.subplot(2,2,4),plt.imshow(sobel,cmap = 'gray')
# plt.title('Sobel'), plt.xticks([]), plt.yticks([])

plt.show()

plt.subplot(2,2,4),plt.imshow(sobel2,cmap = 'gray')
plt.title('Sobel'), plt.xticks([]), plt.yticks([])
plt.show()

##Prewitt
kernelx = np.array([[1,1,1],[0,0,0],[-1,-1,-1]])
kernely = np.array([[-1,0,1],[-1,0,1],[-1,0,1]])
img_prewittx = cv2.filter2D(gaussian2, -1, kernelx)
img_prewitty = cv2.filter2D(gaussian2, -1, kernely)
prewitt2 = img_prewittx+img_prewitty

plt.subplot(121),plt.imshow(img_prewittx,cmap = 'gray')
plt.title('img_prewitt x'), plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(img_prewitty,cmap = 'gray')
plt.title('img_prewitt y'), plt.xticks([]), plt.yticks([])

plt.show()


##Canny
canny2 = cv2.Canny(gaussian2,400,800)

plt.subplot(121),plt.imshow(gaussian2,cmap = 'gray')
plt.title('Original Image'), plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(canny2,cmap = 'gray')
plt.title('Edge Image'), plt.xticks([]), plt.yticks([])

plt.show()


cv2.imwrite("C:/Users/Major Boombi/Desktop/image-processing/Hw1-solution/output/Hw1_2_5809610420_13.JPG", sobel2)
cv2.imwrite("C:/Users/Major Boombi/Desktop/image-processing/Hw1-solution/output/Hw1_2_5809610420_14.JPG", prewitt2)
cv2.imwrite("C:/Users/Major Boombi/Desktop/image-processing/Hw1-solution/output/Hw1_2_5809610420_15.JPG", canny2)


# In[92]:


## Sobel
laplacian = cv2.Laplacian(median3,cv2.CV_64F)

sobelx = cv2.Sobel(median3,cv2.CV_64F,1,0,ksize=5)

sobely = cv2.Sobel(median3,cv2.CV_64F,0,1,ksize=5)

sobel3 = sobelx+sobely 

plt.subplot(2,2,1),plt.imshow(median3,cmap = 'gray')
plt.title('Original'), plt.xticks([]), plt.yticks([])
plt.subplot(2,2,2),plt.imshow(laplacian,cmap = 'gray')
plt.title('Laplacian'), plt.xticks([]), plt.yticks([])
plt.subplot(2,2,3),plt.imshow(sobelx,cmap = 'gray')
plt.title('Sobel X'), plt.xticks([]), plt.yticks([])
plt.subplot(2,2,4),plt.imshow(sobely,cmap = 'gray')
plt.title('Sobel Y'), plt.xticks([]), plt.yticks([])
# plt.subplot(2,2,4),plt.imshow(sobel,cmap = 'gray')
# plt.title('Sobel'), plt.xticks([]), plt.yticks([])

plt.show()

plt.subplot(2,2,4),plt.imshow(sobel3,cmap = 'gray')
plt.title('Sobel'), plt.xticks([]), plt.yticks([])
plt.show()

##Prewitt
kernelx = np.array([[1,1,1],[0,0,0],[-1,-1,-1]])
kernely = np.array([[-1,0,1],[-1,0,1],[-1,0,1]])
img_prewittx = cv2.filter2D(median3, -1, kernelx)
img_prewitty = cv2.filter2D(median3, -1, kernely)
prewitt3 = img_prewittx+img_prewitty

plt.subplot(121),plt.imshow(img_prewittx,cmap = 'gray')
plt.title('img_prewitt x'), plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(img_prewitty,cmap = 'gray')
plt.title('img_prewitt y'), plt.xticks([]), plt.yticks([])

plt.show()


##Canny
canny3 = cv2.Canny(median3,50,150)

plt.subplot(121),plt.imshow(median3,cmap = 'gray')
plt.title('Original Image'), plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(canny3,cmap = 'gray')
plt.title('Edge Image'), plt.xticks([]), plt.yticks([])

plt.show()

cv2.imwrite("C:/Users/Major Boombi/Desktop/image-processing/Hw1-solution/output/Hw1_2_5809610420_16.JPG", sobel3)
cv2.imwrite("C:/Users/Major Boombi/Desktop/image-processing/Hw1-solution/output/Hw1_2_5809610420_17.JPG", prewitt3)
cv2.imwrite("C:/Users/Major Boombi/Desktop/image-processing/Hw1-solution/output/Hw1_2_5809610420_18.JPG", canny3)


# In[93]:


## Write output
# cv2.imwrite("C:/Users/Major Boombi/Desktop/image-processing/Hw1-solution/output/Hw1_2_5809610420_13.JPG", sobel1)
# cv2.imwrite("C:/Users/Major Boombi/Desktop/image-processing/Hw1-solution/output/Hw1_2_5809610420_14.JPG", prewitt1)
# cv2.imwrite("C:/Users/Major Boombi/Desktop/image-processing/Hw1-solution/output/Hw1_2_5809610420_15.JPG", canny1)

# cv2.imwrite("C:/Users/Major Boombi/Desktop/image-processing/Hw1-solution/output/Hw1_2_5809610420_13.JPG", sobel2)
# cv2.imwrite("C:/Users/Major Boombi/Desktop/image-processing/Hw1-solution/output/Hw1_2_5809610420_14.JPG", prewitt2)
# cv2.imwrite("C:/Users/Major Boombi/Desktop/image-processing/Hw1-solution/output/Hw1_2_5809610420_15.JPG", canny2)

# cv2.imwrite("C:/Users/Major Boombi/Desktop/image-processing/Hw1-solution/output/Hw1_2_5809610420_16.JPG", sobel3)
# cv2.imwrite("C:/Users/Major Boombi/Desktop/image-processing/Hw1-solution/output/Hw1_2_5809610420_17.JPG", prewitt3)
# cv2.imwrite("C:/Users/Major Boombi/Desktop/image-processing/Hw1-solution/output/Hw1_2_5809610420_18.JPG", canny3)


# In[ ]:





# In[ ]:




